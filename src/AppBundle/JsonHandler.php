<?php

namespace AppBundle;

use Symfony\Component\Validator\Exception\RuntimeException;

class JsonHandler {

    /**
     * @var array
     */
    protected static $_messages = array(
        JSON_ERROR_NONE           => 'No error has occurred',
        JSON_ERROR_DEPTH          => 'The maximum stack depth has been exceeded',
        JSON_ERROR_STATE_MISMATCH => 'Invalid or malformed JSON',
        JSON_ERROR_CTRL_CHAR      => 'Control character error, possibly incorrectly encoded',
        JSON_ERROR_SYNTAX         => 'Syntax error',
        JSON_ERROR_UTF8           => 'Malformed UTF-8 characters, possibly incorrectly encoded',
    );

    /**
     * @param $value
     * @param int $options
     * @return string
     */
    public static function encode($value, $options = JSON_UNESCAPED_UNICODE) {
        $result = json_encode($value, $options);
        return $result;
//        throw new RuntimeException(static::$_messages[json_last_error()]);
    }

    /**
     * @param string $json
     * @param bool   $assoc
     * @return array
     */
    public static function decode($json, $assoc = true) {
        $result = json_decode($json, $assoc);
        return $result;
//        throw new RuntimeException(static::$_messages[json_last_error()]);
    }

}
