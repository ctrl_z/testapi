<?php

namespace AppBundle;

use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Exception\AMQPTimeoutException;
use PhpAmqpLib\Exception\AMQPProtocolChannelException;
use AppBundle\Exception\IRabbitException;
use Psr\Log\LoggerInterface;
use AppBundle\Services\IncomingMessageManager;
use Symfony\Component\Debug\ExceptionHandler;
use Symfony\Component\Validator\Constraints\DateTime;
use Exception;

/**
 * Класс реализует функции демона (продолжительного процесса) для прослушки очереди RabbitMq
 * Время работы демона должно быть прописано в настройках parameters.yml в секции rabbitmq
 *  - looploop_interval - в секундах время ожидания пакета для одной итерации
 *  - restart_timeout - в секундах время для перезапуска демона
 *
 * Class RabbitMqListener
 * @package AppBundle\RabbitMq
 */
class RabbitMqDaemon
{
    /**
     * @var LoggerInterface
     */
    private $logger;


    /**
     * @var string
     */
    private $queue;

    /**
     * Construct
     *
     * @param IncomingMessageManager $manager
     * @param LoggerInterface $logger
     */
    public function __construct(IncomingMessageManager $manager, LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->manager = $manager;
    }

    /**
     * Метод в котором реализован зацикленный опрос очереди.
     */
    public function loop()
    {
        if($this->manager->getConfig()->getIntegrationStatus() == false){
            echo "Integration switched off in configuration.\r\n";
            sleep(15);
            exit();
        }

        /* Конфигурация rabbitMq очередей для прослушки */
        $cfg = $this->manager->getConfig()->getListenConfig();
        $queue = $cfg['queues']['elma_portal'];
        $this->queue = $queue;

        $loopInterval = intval($cfg['loop_interval']);
        $restartTimeout = intval($cfg['restart_timeout']);
        $timeout = $restartTimeout*60;

        $startTime = time();
        $shutdownTime = $startTime+$timeout;

        echo "Daemon started at: ", (new \DateTime())->setTimestamp($startTime)->format('Y-m-d H:i:s'), "\r\n";
        echo "Planned shutdown time: ", (new \DateTime())->setTimestamp($shutdownTime)->format('Y-m-d H:i:s'), "\r\n";
        echo "Listening queue: ".$queue."\r\n";

        $ch = $this->manager->getChannel();
        try {
            $ch->basic_consume($queue, "", false, false, true, false, array($this, 'processMessage'));
        }catch(AMQPProtocolChannelException $e){
            sleep(3);
            $stopTime = date_create();
            echo "Queue \"".$queue."\" in private use \r\n";
            echo "Daemon shutdown at: ".$stopTime->format('Y-m-d H:i:s')."\r\n";
            exit();
        }

        while(count($ch->callbacks)) {
            try {
                $time = new \DateTime();
                echo $time->format("Y-m-d H:i:s")." - Waiting for incoming messages...\r\n";
//                $this->logger->info("Waiting for incoming messages...");
                $ch->wait(null, false, $loopInterval);
            }catch(AMQPTimeoutException $e){
                if(time() > $shutdownTime){
                    break;
                }
            }
        }

        $ch->close();
        $stopTime = date_create();
        echo "Daemon shutdown at: ".$stopTime->format('Y-m-d H:i:s')."\r\n";
    }

    private $counter = 0;

    /**
     * Метод реализующий передачу сообщений на обработку менеджеру.
     * Также реализовано логирование ошибок.
     * @param AMQPMessage $msg
     */
    public function processMessage(AMQPMessage $msg)
    {
        if($this->counter > 0){
            echo $this->counter." ";
            $this->counter = $this->counter - 1;
            $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
            return;
        }

//        echo "In processMessage...\r\n";
        try {
            $this->manager->processMessage($msg);
            $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
        }catch (IRabbitException $e){
            // Ошибка поймана объектами обработки, нужно оповестить
            if($this->manager->getConfig()->isLoggingEnabled()) {
                $this->manager->notifyMessageError($msg, $e->getMessage(), $this->queue);
                echo $e->getMessage() . "\r\n";
                echo "Error notification sent.\r\n";
            }
            $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
        }
        catch (Exception $e) {
            // Ошибка вылетела и не обработака, оповещаем
            if($this->manager->getConfig()->isLoggingEnabled()) {
                $excMsg = "Caught Exception ('{$e->getMessage()}')\r\n{$e}\r\n";
                $this->manager->notifyMessageError($msg, $excMsg, $this->queue);
                echo $excMsg;
                echo "Error notification sent.\r\n";
            }
            $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
            sleep(3);
            exit(1);
        }
    }
}