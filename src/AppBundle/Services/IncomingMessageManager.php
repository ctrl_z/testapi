<?php


namespace AppBundle\Services;

use AppBundle\MessageHandlerMap;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Psr\Log\LoggerInterface;
use AppBundle\Exceptions\MessageNotValidException;
use AppBundle\IncomingMessage;

/**
 * Менеджер управляющий получением сообщений в виде json
 * Class IncomingMessageManager
 */
class IncomingMessageManager
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Construct
     *
     * @param KernelInterface $container
     * @param LoggerInterface $logger
     */
    public function __construct(ContainerInterface $container, LoggerInterface $logger)
    {
        $this->container = $container;
        $this->logger = $logger;
    }

    /**
     * Метод для первичной обработки сообщения, подготовки и передачи на обработку другим объектам
     * Process message
     * @param string $message
     * @return array
     */
    public function processMessage($message)
    {
        try{
            $msg = new IncomingMessage($message);
            $text = $msg->getText();
            $methods = $msg->getMethods();

            foreach($methods as $m)
            {
                $handler = $this->getTextHandler($m);
                $text = $handler->handle($text);
            }

            $ret = array(
                'status' => 200,
                'text' => $text
            );
        }catch (\Exception $e){
            $ret = array(
                'status' => 503,
                'text' => $e->getMessage()
            );
        }
        return $ret;
    }

    /**
     * Получение объекта, который обрабатывает текст сообщения
     * @param string $method
     * @return object
     */
    public function getTextHandler($method)
    {
        $handlerClass = MessageHandlerMap::getClassName($method);
        $handler = clone($this->container->get($handlerClass));
        return $handler;
    }
}