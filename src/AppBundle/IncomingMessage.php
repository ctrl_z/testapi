<?php

namespace AppBundle;

use AppBundle\JsonHandler;
use AppBundle\Exceptions\MessageNotValidException;

/**
 * Класс для первичной валидации сообщения и заполнение свойств text, methods
 * которые потом пригодятся для обработки другим объектом
 * Class IncomingMessage
 * @package AppBundle
 */
class IncomingMessage
{

    private $text;
    private $methods;

    public function __construct($msg)
    {
        $body = JsonHandler::decode($msg);
        $this->validateFormat($body);

        $this->text = $body['text'];
        $this->methods = $body['methods'];
    }

    public function getText()
    {
        return $this->text;
    }

    public function getMethods()
    {
        return $this->methods;
    }

    private function validateFormat($body)
    {
        if(is_array($body) == false){
            throw new MessageNotValidException('Message not valid. Not found array after unserialisation.');
        }

        if(array_key_exists("job", $body) == false){
            throw new MessageNotValidException('Message not valid. Not found array key "job".');
        }

        if(array_key_exists("text", $body['job']) == false){
            throw new MessageNotValidException('Message not valid. Not found array key "job"->"text".');
        }

        if(array_key_exists("methods", $body['job']) == false){
            throw new MessageNotValidException('Message not valid. Not found array key "job"->"methods".');
        }

        $this->validateMethods($body['job']['methods']);
    }

    private function validateMethods($methods)
    {
        $keys = array_keys(MessageHandlerMap::$map);
        $notExistMethods = [];
        foreach($methods as $m)
        {
            if(in_array($m, $keys) !== false)
            {
                $notExistMethods[] = $m;
            }
        }

        if(count($notExistMethods))
        {
            throw new MessageNotValidException(
                'Message not valid. Methods "'.implode(",", $notExistMethods).'" not exists.'
            );
        }
    }
}