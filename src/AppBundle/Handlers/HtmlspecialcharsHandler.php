<?php

namespace AppBundle\Handlers;


/**
 * Class HtmlspecialcharsHandler
 * @package AppBundle\Handlers
 */
class HtmlspecialcharsHandler implements BaseHandler
{

    public function handle($text)
    {
        return htmlspecialchars($text);
    }
}