<?php

namespace AppBundle\Handlers;


/**
 * Class StripTagsHandler
 * @package AppBundle\Handlers
 */
class StripTagsHandler implements BaseHandler
{
    public function handle($text)
    {
        return strip_tags($text);
    }
}