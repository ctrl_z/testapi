<?php

namespace AppBundle\Handlers;


/**
 * Class ReplaceSpacesToEolHandler
 * @package AppBundle\Handlers
 */
class ReplaceSpacesToEolHandler implements BaseHandler
{

    public function handle($text)
    {
        $characters = array("\r\n", "\n\r", "\n", "\r");
        return str_replace($characters, "\r\n", $text);
    }
}