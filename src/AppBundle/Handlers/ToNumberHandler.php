<?php

namespace AppBundle\Handlers;


/**
 * Class ToNumberHandler
 * @package AppBundle\Handlers
 */
class ToNumberHandler implements BaseHandler
{

    public function handle($text)
    {
        $ret = null;
        preg_match_all('!\d+!', $text, $matches);
        if($matches)
        {
            $ret = $matches[0];
        }
        return $ret;
    }
}