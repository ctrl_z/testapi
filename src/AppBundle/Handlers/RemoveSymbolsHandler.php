<?php

namespace AppBundle\Handlers;


/**
 * Class RemoveSymbolsHandler
 * @package AppBundle\Handlers
 */
class RemoveSymbolsHandler implements BaseHandler
{

    public function handle($text)
    {
        $symbols = '.,/!@#$%&*()';
        return str_replace(explode('', $symbols), '', $text);
    }
}