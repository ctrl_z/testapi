<?php

namespace AppBundle\Handlers;


/**
 * Class RemoveSpacesHandler
 * @package AppBundle\Handlers
 */
class RemoveSpacesHandler implements BaseHandler
{

    public function handle($text)
    {
        return str_replace(' ', '', $text);
    }
}