<?php

namespace AppBundle\Handlers;

use AppBundle\IncomingMessage;

/**
 * Interface BaseHandler
 * @package AppBundle\Handlers
 */
interface BaseHandler
{

    public function handle($msg);

}