<?php

namespace AppBundle;

/**
 * Карта сообщений
 * Class MessageHandlerMap
 * @package AppBundle
 */
class MessageHandlerMap
{
    /**
     * Формат method -> class
     * method - из сообщения
     * class - класс для обработки
     */
    public static $map = array(
        "stripTags" => "StripTagsHandler",
        "removeSpaces" => "RemoveSpacesHandler",
        "replaceSpacesToEol" => "ReplaceSpacesToEolHandler",
        "htmlspecialchars" => "HtmlspecialcharsHandler",
        "removeSymbols" => "RemoveSymbolsHandler",
        "toNumber" => "ToNumberHandler"
    );

    /**
     * @param string $method
     * @return string
     */
    public static function getClassName($method){
        if(array_key_exists($method, self::$map)){
            return self::$map[$method];
        }
        return false;
    }

}