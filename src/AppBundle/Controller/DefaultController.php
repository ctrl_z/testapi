<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $mngr = $this->container->get('AppBundle\Services\IncomingMessageManager');
        $content = $mngr->processMessage($request->getContent());
        $data = $mngr->processMessage($content);
        return JsonResponse::create(array("text" => $data['text']), $data['status']);
    }
}
